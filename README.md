# job-harvester

Dedicated to HR specialists and skilled job seekers

job-harvester connects to job boards and collects postings 

job-harvester can be used to keep track of job postings for specific roles within a specific area

job-harvester will be used by job-board : a friendly dashboard dedicated to HR specialists and skilled job seekers

## setting yourself up (linux)

- after having cloned/forked the repo to your machine, it is best to put in a virtual environnement (venv)

- have python3, pip3 and virtual environnement installed on your machine

    - check if pip3 is installed : ``$ pip3 -h``
    - install virtualenv : ``$ pip3 install virtualenv``
    - in the working directory, create a virtual environnement named _venv_: ``$ virtualenv venv``
    - activate your virtual environnement before installing dependencies: ``$ source venv/activate/activate`` (linux) 
        - notice the ``(venv)`` before the command prompt: it tells you the virtual environnement named _venv_ is currently active

- install these dependencies : 

    - BeautifulSoup : ``$ pip3 install beautifulsoup4``
    - lxml : ``$ pip3 install lxml``
    - requests : ``$ pip3 install requests``

## execution

BEFORE FIRST EXECUTION : make sure to uncomment line 22 ```db.create_database()```
in order for a database file to be created in the working directory

use command ``$ python3 job_scraper.py`` to run the programm

- notice programm will output two files : 

    - ``*.db`` which is a sqlite database viewable thru sqlite3 console or a web-based sqlite viewer. MAKE SURE THIS HAPPEN ONLY ON FIRST EXECUTION !

    - in /logs directory : ``*.out`` which is a log file of what went right or wrong.  You can set logging verbosity level by adding an argument when executing

