from qc_glassdoor_parser import GlassdoorQcParser
from indeed_parser import IndeedParser
from sql_database_operations import Database
from sys import argv
from logger import Logger
from utils import tupleToString

STEP_JOBBOOM_AND_INDEED = "Getting jobs from Jobbom and Indeed"
STEP_GLASSDOOR = "Getting jobs from Glassdoor"

program_name = argv[0]

# trigger DEBUG logging mode by adding integer param 10 in Terminal
# this way :  $python job_scraper.py 10
#
logger_verbosity_level = int(argv[1]) if len(argv) > 1 else Logger.LOWEST_VERBOSITY
logger = Logger(program_name, logger_verbosity_level)

try:    
    db = Database()
    # uncomment on first execution : will create a database file and a table
    # db.create_database()
    # OTHERWISE : ALWAYS KEEP THIS LINE COMMENTED
except Exception as ex:
    logger.error('unable to create database object : ' + tupleToString(ex.args))

try:
    print('parsing INDEED website', end = "\r\n")

    parser = IndeedParser(db, logger)
    parser.execute_and_save()
    
    logger.debug('finished scraping Indeed website' )
except Exception as ex:
    logger.error('unable to execute INDEED : ' + tupleToString(ex.args))

try:
    print(STEP_GLASSDOOR, end = "\r\n")
    glassdoor_parser = GlassdoorQcParser(db)
    glassdoor_parser.execute_and_save()

    logger.debug('finished scraping Glassdoor website' )
except Exception as ex:
    logger.error('Error scraping Glassdoor website : ' + tupleToString(ex.args))

try:
    print("Getting rid of duplicate rows", end = "\r\n")
    db.enforce_integrity()

    logger.debug('finished getting rid of duplicate rows' )
except Exception as ex:
    logger.error('getting rid of duplicate rows : ' + tupleToString(ex.args))

try:
    print("Removing irrelevant job offers", end = "\r\n")
    db.remove_probably_irrelevant_offers()

    logger.debug('finished removing irrelevant offers' )
except Exception as ex:
    logger.error('removing probably irrelevant offers : ' + tupleToString(ex.args))
